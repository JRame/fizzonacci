export class FizzBuzzDefaultOptions {
    public static divisorOne = 3;
    public static divisorOnePhrase = 'Fizz';
    public static divisorTwo = 5;
    public static divisorTwoPhrase = 'Buzz';
}