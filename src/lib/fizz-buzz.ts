import { IOptions } from "./options";
import { FizzBuzzDefaultOptions } from "@/lib/fizz-buzz-default-options";

export class FizzBuzz {
	public readonly divisorOne: number;
	public readonly divisorOnePhrase: string;
	public readonly divisorTwo: number;
	public readonly divisorTwoPhrase: string;

	constructor(options: IOptions) {
		this.divisorOne = options.divisorOne || FizzBuzzDefaultOptions.divisorOne
		this.divisorOnePhrase = options.divisorOnePhrase || FizzBuzzDefaultOptions.divisorOnePhrase; 
		this.divisorTwo = options.divisorTwo || FizzBuzzDefaultOptions.divisorTwo;
		this.divisorTwoPhrase = options.divisorTwoPhrase || FizzBuzzDefaultOptions.divisorTwoPhrase;
	}

    public translate(value: number): string {
		if (value === 0) { return '1'; }

		if (value % this.divisorOne == 0 && value % this.divisorTwo == 0)
			return `${this.divisorOnePhrase}${this.divisorTwoPhrase}`;
		else if (value % this.divisorTwo == 0)
			return this.divisorTwoPhrase;
		else if (value %  this.divisorOne == 0)
			return this.divisorOnePhrase;

		return value.toString();
   }
}