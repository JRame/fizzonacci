import { FizzBuzz } from "./fizz-buzz";

export class FibonacciSequence {
	public getSequence(number: number, y: number, z: number, formatter: FizzBuzz): string[] {
		y = y > 0 ? y : 1;
		z = z > 0 ? z : 2;
		const stringSequence: string[] = [];
		const numberSequence: number[] = [];
		
		for (let i = 0; i < number; i++) {
			if (!numberSequence[y] && !numberSequence[z]) { 
				numberSequence[i] = 1;
				stringSequence[i] = formatter.translate(1); 
			} else {							
				const valueOne = Number(numberSequence[i - y])
				const valueTwo =  Number(numberSequence[i - z]);
				const value = valueOne + valueTwo;

				numberSequence[i] = value;
				stringSequence[i] = formatter.translate(Number(value));
			}
		}

		return stringSequence;
	}
}